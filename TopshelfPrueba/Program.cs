﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using Topshelf;



namespace TopshelfPrueba
{

    class Program
    {
        static void Main(string[] args)
         {
            HostFactory.Run(x =>
            {
                x.Service<Webserver>(s =>
                {
                    s.ConstructUsing(name => new Webserver());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();
                x.SetDescription("Demo using TopShelf");
                x.SetDisplayName("TopShelf Demo");
                x.SetServiceName("TopShelfDemo");
            });
         }
     }
}
