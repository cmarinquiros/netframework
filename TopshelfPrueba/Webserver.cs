﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace TopshelfPrueba
{
    public class Webserver
    {
        private IDisposable _webapp;

        public void Start()
        {
            _webapp = WebApp.Start<Starup>("http://localhost:9000");
        }
        public void Stop()
        {
            _webapp?.Dispose();
        }
    }
}
