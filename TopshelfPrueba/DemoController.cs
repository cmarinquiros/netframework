﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace TopshelfPrueba
{
    public class DemoController : ApiController
    {
        int i=0;
        //Get api/demo
        public IEnumerable<string> Get()
        {
            return new string[] { "Hello", "World" };
        }
        //GET api/demo/5 
        public string Get(int id)
        {
            return "Hello, World!";
        }
    }
}
